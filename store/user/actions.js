import { SIGN_IN, SIGN_OUT } from './types';

export const signIn = ({ googleId, name }) => ({
  type: SIGN_IN,
  name,
  googleId,
});

export const signOut = () => ({
  type: SIGN_OUT,
});
