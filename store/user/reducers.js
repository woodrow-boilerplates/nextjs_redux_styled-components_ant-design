import { SIGN_IN, SIGN_OUT } from './types';

const initialState = {
  name: null,
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case SIGN_IN:
      return {
        ...state,
        name: action.name,
      };
    case SIGN_OUT:
      return {
        ...state,
        name: null,
      };
    default:
      return state;
  }
};

export default user;
