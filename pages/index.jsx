import React from 'react';
import Head from 'next/head';
import MainWrapper from '../components/wrappers/MainWrapper';

export default function Home() {
  return (
    <>
      <Head>
        <title>Boilerplate</title>
      </Head>
      <MainWrapper>
        Bolerplate
      </MainWrapper>
    </>
  );
}
