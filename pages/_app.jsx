import React from 'react';
import { Provider as StoreProvider } from 'react-redux';

import { useStore } from '../store';

/* eslint-disable react/jsx-props-no-spreading */
export default function App({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState);
  return (
    <StoreProvider store={store}>
      <Component {...pageProps} />
    </StoreProvider>
  );
}
